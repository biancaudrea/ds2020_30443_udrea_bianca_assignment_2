package ro.tuc.ds2020.controller;


import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ro.tuc.ds2020.producer.Producer;

import java.io.File;
import java.io.FileNotFoundException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;


@RestController
@RequestMapping(value = "/send")
public class RabbitMQWebController {

    @Autowired
    Producer producer;
//
//    @GetMapping(value = "/producer")
//    public String producer(@RequestParam("empName") String empName,@RequestParam("empId") String empId) {
//
//        Employee emp=new Employee();
//        emp.setEmpId(empId);
//        emp.setEmpName(empName);
//        producer.send(emp);
//
//        return "Message sent to the RabbitMQ JavaInUse Successfully";
//    }

    @GetMapping("")
    public String sendMessage(@RequestParam("msg") String msg){

       // System.out.println("*******" + msg);
      //  for(int i=0; i<25;i++)

        return "Successfully sent message: "+msg;
    }



}