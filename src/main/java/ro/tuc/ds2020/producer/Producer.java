package ro.tuc.ds2020.producer;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.messages.CustomMessage;


@Service
public class Producer {

    @Autowired
    private AmqpTemplate rabbitTemplate;

    @Value("${rabbitmq.queue}")
    private String queueName;

    @Value("${rabbitmq.exchange}")
    private String exchange;

    @Value("${rabbitmq.routingkey}")
    private String routingkey;

    public void send(CustomMessage customMessage) {
        rabbitTemplate.convertAndSend(exchange, routingkey, customMessage);
        System.out.println("Send msg = " + customMessage);

    }
}