package ro.tuc.ds2020.threads;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ro.tuc.ds2020.messages.CustomMessage;
import ro.tuc.ds2020.producer.Producer;

import java.io.File;
import java.io.FileNotFoundException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

@Component
@Scope("prototype")
public class MainThread implements Runnable {
    @Autowired
    Producer producer;

    @Override
    public void run() {

        try {
            File myObj = new File("activity.txt");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                String data = myReader.nextLine();
                String[] array = data.split("\\t+", 3);
                for (String a : array)
                    System.out.println(a);
                sendJson(array);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    private void sendJson(String[] array)
    {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        CustomMessage customMessage = new CustomMessage("13ae8092-2956-49a1-9270-2ed8eee9878c", LocalDateTime.parse(array[0], formatter).toInstant(ZoneOffset.ofTotalSeconds(0)).toEpochMilli(),LocalDateTime.parse(array[1], formatter).toInstant(ZoneOffset.ofTotalSeconds(0)).toEpochMilli(),array[2].replace("\t", ""));

        producer.send(customMessage);
    }
}
