package ro.tuc.ds2020;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.task.TaskExecutor;
import ro.tuc.ds2020.threads.MainThread;

@SpringBootApplication
public class Ds2020Application {


	public static void main(String[] args) {
		SpringApplication.run(Ds2020Application.class, args);

	}

}
